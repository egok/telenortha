package com.egok.telenortha.service;

import com.egok.telenortha.dto.ProductDto;
import java.util.List;

public interface ProductsService {
    List<ProductDto> getProducts(String product_type,
                                 Double min_price,
                                 Double max_price,
                                 String city,
                                 String color,
                                 Integer gb_limit_min,
                                 Integer gb_limit_max);
}
