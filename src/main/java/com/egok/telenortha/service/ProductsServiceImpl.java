package com.egok.telenortha.service;

import com.egok.telenortha.dto.ProductDto;
import com.egok.telenortha.entities.Product;
import com.egok.telenortha.repositories.ProductRepository;
import com.egok.telenortha.utils.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    ProductRepository productRepo;

    String gbPrefix = "gb_limit:";

    @Override
    public List<ProductDto> getProducts(String product_type,
                                        Double min_price,
                                        Double max_price,
                                        String city,
                                        String color,
                                        Integer gb_limit_min,
                                        Integer gb_limit_max) {

        String colorWithPrefix = color != null ? "color:" + color : null;
        String gbMinWithPrefix = gb_limit_min != null ? "gb_limit:" + gb_limit_min : null;
        String gbMaxWithPrefix = gb_limit_max != null ? "gb_limit:" + gb_limit_max : null;
        String cityToLowerCase = city != null ? city.toLowerCase() : null;
        String productTypeToLowerCase = product_type != null ? product_type.toLowerCase() : null;
        String gbPrefix = "gb_limit";

        List<Product> entities = productRepo.findByTypePropertiesPriceAndCity(productTypeToLowerCase,
                colorWithPrefix,
                min_price,
                max_price,
                cityToLowerCase,
                gbMinWithPrefix,
                gbMaxWithPrefix,
                gbPrefix);
        List<Product> filteredEntities = filterGbLimit(entities, gb_limit_min, gb_limit_max);
        return filteredEntities.stream().map(entity -> convertEntityToDto(entity)).collect(Collectors.toList());
    }

    private List<Product> filterGbLimit(List<Product> entities, Integer gbMin, Integer gbMax) {
        if (entities != null && entities.size() > 0) {
            return entities.stream().filter(entity -> {
                if (entity.getProps() != null && (gbMin != null || gbMax != null)) {
                    String gbStringValue = entity.getProps().substring(gbPrefix.length());
                    if (Utilities.isInt(gbStringValue)) {
                        Integer gbValue = Integer.valueOf(gbStringValue);
                        if (gbMin != null && gbMax != null)
                            return gbValue >= gbMin && gbValue <= gbMax;
                        else if (gbMin != null)
                            return gbValue >= gbMin;
                        else if (gbMax != null)
                            return gbValue <= gbMax;
                    }
                    return false;
                }
                return true;
            }).collect(Collectors.toList());
        }
        return entities;
    }

    private ProductDto convertEntityToDto(Product entity) {
        ProductDto dto = new ProductDto();
        dto.setPrice(entity.getPrice());
        dto.setProperties(entity.getProps());
        dto.setStore_address(entity.getAddress());
        dto.setType(entity.getType());
        return dto;
    }

}
