package com.egok.telenortha.dto;

import java.util.List;

public class ProductWrapper {
    private List<ProductDto> data;

    public ProductWrapper() {
    }

    public ProductWrapper(List<ProductDto> data) {
        super();
        this.data = data;
    }

    public List<ProductDto> getData() {
        return data;
    }

    public void setData(List<ProductDto> data) {
        this.data = data;
    }
}
