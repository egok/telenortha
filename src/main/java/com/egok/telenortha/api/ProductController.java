package com.egok.telenortha.api;

import com.egok.telenortha.dto.ProductDto;
import com.egok.telenortha.dto.ProductWrapper;
import com.egok.telenortha.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class ProductController {

    private final ProductsService productsService;

    @Autowired
    public ProductController(ProductsService productsService){
        this.productsService = productsService;
    }

    @GetMapping("/product")
    public ResponseEntity<Object> queryProducts(@RequestParam(name = "type", required = false) String product_type,
                                                @RequestParam(name = "min_price", required = false) Double min_price,
                                                @RequestParam(name = "max_price", required = false) Double max_price,
                                                @RequestParam(name = "city", required = false) String city,
                                                @RequestParam(name = "color", required = false) String color,
                                                @RequestParam(name = "gb_limit_min", required = false) Integer gb_limit_min,
                                                @RequestParam(name = "gb_limit_max", required = false) Integer gb_limit_max) {

        List<ProductDto> products = productsService.getProducts(product_type,
                min_price,
                max_price,
                city,
                color,
                gb_limit_min,
                gb_limit_max);

        return ResponseEntity.accepted().body(new ProductWrapper(products));
    }
}
