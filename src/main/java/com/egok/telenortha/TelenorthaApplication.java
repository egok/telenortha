package com.egok.telenortha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.egok"})
public class TelenorthaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelenorthaApplication.class, args);
	}

}
