package com.egok.telenortha.repositories;

import com.egok.telenortha.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByType(String type);

    @Query("select p from Product p where "
            + "(:type is null or LOWER(type)=:type) "
            + " and ((:minPrice is null or price >=:minPrice) "
            + "and (:maxPrice is null or price <=:maxPrice)) "
            + " and (:city is null or LOWER(address) like %:city%) "
            + " and (:color is null or LOWER(props) like %:color%) "
            + " and (:gbMin is null or props like %:gbPrefix% ) "
            + "and (:gbMax is null or props like %:gbPrefix% )")
    List<Product> findByTypePropertiesPriceAndCity(@Param("type") String type,
                                                   @Param("color") String color,
                                                   @Param("minPrice") Double minPrice,
                                                   @Param("maxPrice") Double maxPrice,
                                                   @Param("city") String city,
                                                   @Param("gbMin") String gbMin,
                                                   @Param("gbMax") String gbMax,
                                                   @Param("gbPrefix") String prefix);

}
